<?php

namespace App\Http\Controllers;

use App\Team;
use App\Http\Requests\StoreTeam;
use App\Http\Requests\UpdateTeam;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response()->json(Team::with('players')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTeam  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeam $request)
    {
        Team::create($request->all());

        return Response()->json(['message' => 'Team Created'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        return Response()->json($team->load('players'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTeam  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeam $request, Team $team)
    {
        $team->update($request->all());

        return Response()->json(['message' => 'Team Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        return Response()->json(['message' => 'Access denied'], 403);
    }
}
