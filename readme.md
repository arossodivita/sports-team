## Sports Team

Simple APi for handle sport teams and their players, the functions included are:

- Add team
- Add player
- Update player
- Get team and its players

Laravel Passport are installed to implement OAuth2 authentication