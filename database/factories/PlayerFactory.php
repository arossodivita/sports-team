<?php

use Faker\Generator as Faker;

/**
 * Create fake player with team relationship
 */

$factory->define(App\Player::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'team_id' => function () {
            return factory(App\Club::class)->create()->id;
        }
    ];
});