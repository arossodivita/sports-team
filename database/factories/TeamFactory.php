<?php

use Faker\Generator as Faker;

/**
 * Create fake team
 */

$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement([
            'Juventus',
            'Milan',
            'Napole',
            'Inter',
            'Lazio',
            'Roma',
            'Torino',
            'Udinese',
            'Bologna',
            'Parma',
        ]),
    ];
});